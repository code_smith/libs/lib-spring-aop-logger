package kz.codesmith.logger;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "kz.codesmith.logger.*")
public class LoggerAutoConfiguration {

}
