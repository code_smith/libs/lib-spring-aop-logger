package kz.codesmith.logger.request;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class RequestIdFilter extends OncePerRequestFilter {

  private final XrequestId requestId;

  @Autowired
  public RequestIdFilter(XrequestId requestId) {
    this.requestId = requestId;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain filterChain
  ) throws ServletException, IOException {
    if (StringUtils.isNotBlank(requestId.get())) {
      response.addHeader(XrequestId.REQUEST_ID_HEADER, requestId.get());
    }
    filterChain.doFilter(request, response);
  }
}
