package kz.codesmith.logger.request;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RequestIdListener implements ServletRequestListener {

  private final XrequestId requestId;

  @Autowired
  RequestIdListener(XrequestId requestId) {
    this.requestId = requestId;
  }

  @Override
  public void requestDestroyed(ServletRequestEvent sre) {
    requestId.clear();
  }

  @Override
  public void requestInitialized(ServletRequestEvent sre) {
    HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
    String headerRequestId = request.getHeader(XrequestId.REQUEST_ID_HEADER);
    if (StringUtils.isNotBlank(headerRequestId)) {
      requestId.set(headerRequestId);
    } else {
      requestId.set();
    }
  }
}
