package kz.codesmith.logger.request;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Component
public class XrequestId {

    public static final String REQUEST_ID_HEADER = "X-Request-Id";
    public static final String REQUEST_ID_PARAM = "reqId";
    public static final String REQUEST_ID_USER = "reqUser";

    private static final DateTimeFormatter dateTimePartFormatter = DateTimeFormatter
        .ofPattern("yyyyMMdd_HHmmss_SSS");

    public String generateRequestId() {
        return LocalDateTime.now().format(dateTimePartFormatter)
            + "_" + RandomStringUtils.randomAlphanumeric(3);
    }

    public String get() {
        return MDC.get(REQUEST_ID_PARAM);
    }

    public void set() {
        set(generateRequestId());
    }

    public void set(String reqId) {
        if (Objects.nonNull(reqId)) {
            MDC.put(REQUEST_ID_PARAM, reqId);
        }
    }

    public String getUsername() {
        return MDC.get(REQUEST_ID_USER);
    }

    public void setUsername(String username) {
        if (Objects.nonNull(username)) {
            MDC.put(REQUEST_ID_USER, username);
        }
    }

    public void clear() {
        MDC.remove(REQUEST_ID_PARAM);
        MDC.remove(REQUEST_ID_USER);
    }

}
