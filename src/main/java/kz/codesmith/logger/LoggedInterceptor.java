package kz.codesmith.logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayDeque;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Order(1000)
@Aspect
@Component
@EnableAspectJAutoProxy
public class LoggedInterceptor {

  private final ObjectMapper objectMapper;

  private final ThreadLocal<ArrayDeque<String>> callStackLocal = new ThreadLocal<>();

  @Autowired
  LoggedInterceptor(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Around("execution(* *(..)) && @within(logged)")
  protected Object aroundInvoke(ProceedingJoinPoint joinPoint, Logged logged) throws Throwable {
    String joinPointName = StringUtils.substringAfterLast(joinPoint.getSignature()
        .getDeclaringTypeName(), ".")
        .concat(".")
        .concat(joinPoint.getSignature().getName());

    log.info(
        "invoke [{}] with args: {}",
        joinPointName,
        objectMapper.writeValueAsString(joinPoint.getArgs())
    );

    ArrayDeque<String> callStack = callStackLocal.get();
    if (callStack == null) {
      callStack = new ArrayDeque<>();
      callStackLocal.set(callStack);
    }
    callStack.addLast(joinPointName);

    long startAt = System.nanoTime();
    try {
      Object result = joinPoint.proceed();
      long timeNanos = System.nanoTime() - startAt;
      if (logged.logResponse()) {
        log.info(
            "invoke [{}] ok in [{} ms] with result: {}",
            joinPointName,
            TimeUnit.NANOSECONDS.toMillis(timeNanos),
            objectMapper.writeValueAsString(result)
        );
      } else {
        log.info(
            "invoke [{}] ok in [{} ms]",
            joinPointName,
            TimeUnit.NANOSECONDS.toMillis(timeNanos)
        );
      }
      return result;
    } catch (Throwable t) {
      long timeNanos = System.nanoTime() - startAt;
      log.error(
          "invoke [{}] err in [{} ms] with error: {} - {}",
          joinPointName,
          TimeUnit.NANOSECONDS.toMillis(timeNanos),
          t.getClass().getSimpleName(),
          t.getMessage(),
          t
      );
      throw t;
    } finally {
      callStack.removeLast();
      if (callStack.isEmpty()) {
        callStackLocal.remove();
      }
    }
  }

}
